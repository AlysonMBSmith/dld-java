package org.dld.chain.block;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.ZonedDateTime;

public class CreateTabulator extends BlockPayload {

    private String tabulatorId;
    private String tabulatorPublicKey;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    private ZonedDateTime expires;

    public CreateTabulator() {
        super(BlockTypes.CREATE_TABULATOR);
    }

    public String getTabulatorId() {
        return tabulatorId;
    }

    public CreateTabulator setTabulatorId(final String tabulatorId) {
        this.tabulatorId = tabulatorId;
        return this;
    }

    public String getTabulatorPublicKey() {
        return tabulatorPublicKey;
    }

    public CreateTabulator setTabulatorPublicKey(final String tabulatorPublicKey) {
        this.tabulatorPublicKey = tabulatorPublicKey;
        return this;
    }

    public ZonedDateTime getExpires() {
        return expires;
    }

    public CreateTabulator setExpires(final ZonedDateTime expires) {
        this.expires = expires;
        return this;
    }

    @Override
    public CreateTabulator setParentBlockSignature(final String parentBlockSignature) {
        super.setParentBlockSignature(parentBlockSignature);
        return this;
    }

    @Override
    public String toString() {
        return "CreateTabulator{" +
                "type=" + type +
                ", parentBlockSignature='" + parentBlockSignature + '\'' +
                ", tabulatorId='" + tabulatorId + '\'' +
                ", tabulatorPublicKey='" + tabulatorPublicKey + '\'' +
                ", expires=" + expires +
                '}';
    }
}
