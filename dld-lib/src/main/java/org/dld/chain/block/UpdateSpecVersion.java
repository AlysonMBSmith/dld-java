package org.dld.chain.block;

public class UpdateSpecVersion extends BlockPayload {

    private String specVersion;

    public UpdateSpecVersion() {
        super(BlockTypes.UPDATE_SPEC_VERSION);
    }

    public String getSpecVersion() {
        return specVersion;
    }

    public UpdateSpecVersion setSpecVersion(final String specVersion) {
        this.specVersion = specVersion;
        return this;
    }

    @Override
    public String toString() {
        return "UpdateSpecVersion{" +
                "type=" + type +
                ", parentBlockSignature='" + parentBlockSignature + '\'' +
                ", specVersion='" + specVersion + '\'' +
                '}';
    }
}
