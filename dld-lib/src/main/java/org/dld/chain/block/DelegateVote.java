package org.dld.chain.block;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.ZonedDateTime;

public class DelegateVote extends BlockPayload {

    private String delegatorId;
    private String delegateId;
    private String proposalSignature;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    private ZonedDateTime delegationEndTime;

    public DelegateVote() {
        super(BlockTypes.DELEGATE_VOTE);
    }

    public String getDelegatorId() {
        return delegatorId;
    }

    public DelegateVote setDelegatorId(final String delegatorId) {
        this.delegatorId = delegatorId;
        return this;
    }

    public String getDelegateId() {
        return delegateId;
    }

    public DelegateVote setDelegateId(final String delegateId) {
        this.delegateId = delegateId;
        return this;
    }

    public String getProposalSignature() {
        return proposalSignature;
    }

    public DelegateVote setProposalSignature(final String proposalSignature) {
        this.proposalSignature = proposalSignature;
        return this;
    }

    public ZonedDateTime getDelegationEndTime() {
        return delegationEndTime;
    }

    public DelegateVote setDelegationEndTime(final ZonedDateTime delegationEndTime) {
        this.delegationEndTime = delegationEndTime;
        return this;
    }

    @Override
    public String toString() {
        return "DelegateVote{" +
                "type=" + type +
                ", parentBlockSignature='" + parentBlockSignature + '\'' +
                ", delegatorId='" + delegatorId + '\'' +
                ", delegateId='" + delegateId + '\'' +
                ", proposalSignature='" + proposalSignature + '\'' +
                ", delegationEndTime=" + delegationEndTime +
                '}';
    }
}
