package org.dld.chain.block;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.EXISTING_PROPERTY,
        property = "type"
)
@JsonSubTypes({
        @JsonSubTypes.Type(value = Genesis.class, name = "GENESIS"),
        @JsonSubTypes.Type(value = CreateRegistrar.class, name = "CREATE_REGISTRAR"),
        @JsonSubTypes.Type(value = UpdateRegistrar.class, name = "UPDATE_REGISTRAR"),
        @JsonSubTypes.Type(value = CreateVoter.class, name = "CREATE_VOTER"),
        @JsonSubTypes.Type(value = UpdateVoter.class, name = "UPDATE_VOTER"),
        @JsonSubTypes.Type(value = CreateTabulator.class, name = "CREATE_TABULATOR"),
        @JsonSubTypes.Type(value = UpdateTabulator.class, name = "UPDATE_TABULATOR"),
        @JsonSubTypes.Type(value = ProposeLegislation.class, name = "PROPOSE_LEGISLATION"),
        @JsonSubTypes.Type(value = DelegateVote.class, name = "DELEGATE_VOTE"),
        @JsonSubTypes.Type(value = VotePayload.class, name = "VOTE"),
        @JsonSubTypes.Type(value = VoteTallyPayload.class, name = "VOTE_TALLY"),
        @JsonSubTypes.Type(value = VoteResultPayload.class, name = "VOTE_RESULT"),
        @JsonSubTypes.Type(value = UpdateSpecVersion.class, name = "UPDATE_SPEC_VERSION")
})
public class BlockPayload {

    protected BlockTypes type;

    protected String parentBlockSignature;

    protected BlockPayload(final BlockTypes type) {
        this.type = type;
    }

    public BlockTypes getType() {
        return type;
    }

    public BlockPayload setType(final BlockTypes type) {
        this.type = type;
        return this;
    }

    public String getParentBlockSignature() {
        return parentBlockSignature;
    }

    public BlockPayload setParentBlockSignature(final String parentBlockSignature) {
        this.parentBlockSignature = parentBlockSignature;
        return this;
    }



}
