package org.dld.chain.block;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.ZonedDateTime;

public class CreateVoter extends BlockPayload {

    private String voterId;
    private String voterPublicKey;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    private ZonedDateTime expires;

    public CreateVoter() {
        super(BlockTypes.CREATE_VOTER);
    }

    public String getVoterId() {
        return voterId;
    }

    public CreateVoter setVoterId(final String voterId) {
        this.voterId = voterId;
        return this;
    }

    public String getVoterPublicKey() {
        return voterPublicKey;
    }

    public CreateVoter setVoterPublicKey(final String voterPublicKey) {
        this.voterPublicKey = voterPublicKey;
        return this;
    }

    public ZonedDateTime getExpires() {
        return expires;
    }

    public CreateVoter setExpires(final ZonedDateTime expires) {
        this.expires = expires;
        return this;
    }

    public CreateVoter setParentBlockSignature(final String parentBlockSignature) {
        super.setParentBlockSignature(parentBlockSignature);
        return this;
    }

    @Override
    public String toString() {
        return "CreateVoter{" +
                "type=" + type +
                ", parentBlockSignature='" + parentBlockSignature + '\'' +
                ", voterId='" + voterId + '\'' +
                ", voterPublicKey='" + voterPublicKey + '\'' +
                ", expires=" + expires +
                '}';
    }
}
