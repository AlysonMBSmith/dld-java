package org.dld.chain.block;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NONE
)
public class Block {

    private BlockPayload payload;
    private String payloadSignature;
    private String writerId;

    public BlockPayload getPayload() {
        return payload;
    }

    public String getPayloadSignature() {
        return payloadSignature;
    }

    public String getWriterId() {
        return writerId;
    }

    public Block setPayload(final BlockPayload payload) {
        this.payload = payload;
        return this;
    }

    public Block setPayloadSignature(final String payloadSignature) {
        this.payloadSignature = payloadSignature;
        return this;
    }

    public Block setWriterId(final String writerId) {
        this.writerId = writerId;
        return this;
    }

    @Override
    public String toString() {
        return "Block{" +
                "payload=" + payload +
                ", payloadSignature='" + payloadSignature + '\'' +
                ", writerId='" + writerId + '\'' +
                '}';
    }

    public static Block create(final BlockPayload payload, final String signature, final String writerId) {
        return new Block()
                .setPayload(payload)
                .setPayloadSignature(signature)
                .setWriterId(writerId);
    }
}
