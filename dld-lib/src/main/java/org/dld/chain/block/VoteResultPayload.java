package org.dld.chain.block;

import java.util.List;

public class VoteResultPayload extends BlockPayload {

    private String tabulatorId;
    private List<String> tabulatorConsensus;
    private boolean proposalPassed;
    private String proposalSignature;

    public VoteResultPayload() {
        super(BlockTypes.VOTE_RESULT);
    }

    public String getTabulatorId() {
        return tabulatorId;
    }

    public VoteResultPayload setTabulatorId(final String tabulatorId) {
        this.tabulatorId = tabulatorId;
        return this;
    }

    public List<String> getTabulatorConsensus() {
        return tabulatorConsensus;
    }

    public VoteResultPayload setTabulatorConsensus(final List<String> tabulatorConsensus) {
        this.tabulatorConsensus = tabulatorConsensus;
        return this;
    }

    public boolean isProposalPassed() {
        return proposalPassed;
    }

    public VoteResultPayload setProposalPassed(final boolean proposalPassed) {
        this.proposalPassed = proposalPassed;
        return this;
    }

    public String getProposalSignature() {
        return proposalSignature;
    }

    public VoteResultPayload setProposalSignature(final String proposalSignature) {
        this.proposalSignature = proposalSignature;
        return this;
    }

    @Override
    public String toString() {
        return "VoteResult{" +
                "type=" + type +
                ", parentBlockSignature='" + parentBlockSignature + '\'' +
                ", tabulatorId='" + tabulatorId + '\'' +
                ", tabulatorConsensus=" + tabulatorConsensus +
                ", proposalPassed=" + proposalPassed +
                ", proposalSignature='" + proposalSignature + '\'' +
                '}';
    }
}
