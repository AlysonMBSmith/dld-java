package org.dld.chain.block;

public class VotePayload extends BlockPayload {

    private String voterId;
    private String proposalSignature;
    private boolean vote;

    public VotePayload() {
        super(BlockTypes.VOTE);
    }

    public String getVoterId() {
        return voterId;
    }

    public VotePayload setVoterId(final String voterId) {
        this.voterId = voterId;
        return this;
    }

    public String getProposalSignature() {
        return proposalSignature;
    }

    public VotePayload setProposalSignature(final String proposalSignature) {
        this.proposalSignature = proposalSignature;
        return this;
    }

    public boolean getVote() {
        return vote;
    }

    public VotePayload setVote(final boolean vote) {
        this.vote = vote;
        return this;
    }

    @Override
    public String toString() {
        return "Vote{" +
                "type=" + type +
                ", parentBlockSignature='" + parentBlockSignature + '\'' +
                ", voterId='" + voterId + '\'' +
                ", proposalSignature='" + proposalSignature + '\'' +
                ", vote=" + vote +
                '}';
    }
}
