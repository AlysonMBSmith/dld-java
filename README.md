# DLD-Java
DLD-Java is a Java-based implementation of the Decentralized Liquid Democracy spec. Currently it only supports a command-line interface that provides all available blockchain operations.

## Commands
* `genkeys` - Generate a public/private keypair named `dld.pub` and `dld.key` respectively.
* Common arguments for the following commands:
  * `--privatekey`, `-k` - (Required) Path to the private key file of the user that is performing this operation
  * `--userid`, `-u` - (Required) Id of the user that is performing this operation
  * `--chainfile`, `-f` - File name for the DLD chain
* `create-chain` - Create a DLD chain
  * `--publickey`, `-p` (Required) Path to the super registrar public key. Must be the public key for the private key used in this operation
  * `--gitrepo`, `-g` (Required) The URI that points to the legislation git repository
  * `--mainbranch`, `-m` (Required) The name of the main branch in the legislation repository
  * `--headhash`, `-c` (Required) The main branch head commit hash
  * `--specversion`, `-s` (Required) The spec version that this chain is compliant to
* `create-registrar` - Add a registrar to the chain
  * `--publickey`, `-p` (Required) Path to the public key file of the registar being added
  * `--regid`, (Required) The ID of the registrar being added to the chain
  * `-s` Set if the registrar being added is a super registrar (Note: Only the genesis super registrar may add other super registrars)
  * `--expiry`, `-e` The optional expiry date-time for the registrar being added
* `create-voter` - Add a voter to the chain
  * `--publickey`, `-p` (Required) Path to the public key file of the voter being added
  * `--voterid` (Required) The ID of the voter being added to the chain
  * `--expiry`, `-e` The optional expiry date-time for the voter being added
* `create-tabulator` - Add a tabulator to the chain
  * `--publickey`, `-p` (Required) Path to the public key file of the tabulator being added
  * `--tabulatorid` (Required) The ID of the tabulator being added to the chain
  * `--expiry`, `-e` The optional expiry date-time for the tabulator being added
