package org.dld.chain.logic;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.dld.app.core.ErrorCodes;
import org.dld.app.data.DldDataStore;
import org.dld.app.json.JacksonConfig;
import org.dld.app.util.CryptoUtil;
import org.dld.chain.block.Block;
import org.dld.chain.block.CreateTabulator;
import org.dld.chain.block.CreateVoter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.ZonedDateTime;

@SpringBootTest(classes = {JacksonConfig.class, CreateTabulatorBlockValidator.class, DldDataStore.class})
public class CreateTabulatorBlockValidatorTest {

    private static PublicKey reg001PublicKey;
    private static PrivateKey genesis001PrivateKey;
    private static PrivateKey reg001PrivateKey;
    private static PublicKey tabulator001PublicKey;
    private static PrivateKey tabulator001PrivateKey;

    @BeforeAll
    static void loadKeys() throws NoSuchAlgorithmException, IOException {
        genesis001PrivateKey =
                CryptoUtil.loadPrivateKey(new ClassPathResource("keypairs/genesis-001/dld.key").getFile())
                        .orElseThrow();
        reg001PublicKey = CryptoUtil.loadPublicKey(new ClassPathResource("keypairs/reg-001/dld.pub").getFile())
                .orElseThrow();
        reg001PrivateKey = CryptoUtil.loadPrivateKey(new ClassPathResource("keypairs/reg-001/dld.key").getFile())
                .orElseThrow();
        tabulator001PublicKey = CryptoUtil.loadPublicKey(new ClassPathResource("keypairs/tabulator-001/dld.pub").getFile())
                .orElseThrow();
        tabulator001PrivateKey = CryptoUtil.loadPrivateKey(new ClassPathResource("keypairs/tabulator-001/dld.key").getFile())
                .orElseThrow();
    }

    @Nested
    @SpringBootTest(classes = {JacksonConfig.class, CreateTabulatorBlockValidator.class, DldDataStore.class})
    class RegistrarAddedChain {

        private final CreateTabulatorBlockValidator blockValidator;
        private final ObjectMapper objectMapper;
        private final String headHash;

        @Autowired
        RegistrarAddedChain(final ObjectMapper objectMapper) throws IOException {
            DldDataStore dldDataStore = new DldDataStore(objectMapper);
            this.blockValidator = new CreateTabulatorBlockValidator(dldDataStore, objectMapper);
            this.objectMapper = objectMapper;

            dldDataStore.loadDldChain(new ClassPathResource("chains/reg001.chain").getFile());
            this.headHash = dldDataStore.getHeadBlockSignature();
        }

        @Test
        @DisplayName("It should successfully create a block when everything is present and valid")
        void createValidBlock() throws JsonProcessingException, NoSuchAlgorithmException {

            CreateTabulator createTabulator = new CreateTabulator()
                    .setTabulatorId("tabulator-001")
                    .setTabulatorPublicKey(CryptoUtil.encodePublicKeyToString(tabulator001PublicKey));

            String signedPayload = CryptoUtil.sign(objectMapper.writeValueAsString(createTabulator), reg001PrivateKey)
                    .orElseThrow();

            Block result = Assertions.assertDoesNotThrow(
                    () -> blockValidator.validate(createTabulator, signedPayload, "reg-001", reg001PublicKey));

            Assertions.assertNotNull(result);
        }

        @Test
        @DisplayName("It should throw an exception if the tabulator id is missing")
        void missingId() throws JsonProcessingException, NoSuchAlgorithmException {
            CreateTabulator createTabulator = new CreateTabulator()
                    .setTabulatorPublicKey(CryptoUtil.encodePublicKeyToString(tabulator001PublicKey));

            String signedPayload = CryptoUtil.sign(objectMapper.writeValueAsString(createTabulator), genesis001PrivateKey)
                    .orElseThrow();

            InvalidBlockException exception = Assertions.assertThrows(InvalidBlockException.class, () -> blockValidator.validate(createTabulator,
                    signedPayload, "reg-001", reg001PublicKey));

            Assertions.assertEquals(ErrorCodes.ENTITY_ID_MUST_BE_SPECIFIED, exception.getErrorCode());
        }

        @Test
        @DisplayName("It should throw an exception if the tabulator public key is missing")
        void missingPublicKey() throws JsonProcessingException, NoSuchAlgorithmException {
            CreateTabulator createTabulator = new CreateTabulator()
                    .setTabulatorId("tabulator-001");

            String signedPayload = CryptoUtil.sign(objectMapper.writeValueAsString(createTabulator), genesis001PrivateKey)
                    .orElseThrow();

            InvalidBlockException exception = Assertions.assertThrows(InvalidBlockException.class, () -> blockValidator.validate(createTabulator,
                    signedPayload, "reg-001", reg001PublicKey));

            Assertions.assertEquals(ErrorCodes.ENTITY_PUBLIC_KEY_MUST_BE_SPECIFIED, exception.getErrorCode());
        }

        @Test
        @DisplayName("It should throw an exception if the user's expiry date is in the past")
        void tabulatorExpiryInThePast() throws JsonProcessingException, NoSuchAlgorithmException {
            CreateTabulator createTabulator = new CreateTabulator()
                    .setTabulatorId("tabulator-001")
                    .setTabulatorPublicKey(CryptoUtil.encodePublicKeyToString(tabulator001PublicKey))
                    .setExpires(ZonedDateTime.of(LocalDateTime.of(2000, Month.JANUARY, 1, 0, 0), ZoneId.systemDefault()));

            String signedPayload = CryptoUtil.sign(objectMapper.writeValueAsString(createTabulator), genesis001PrivateKey)
                    .orElseThrow();

            InvalidBlockException exception = Assertions.assertThrows(InvalidBlockException.class, () -> blockValidator.validate(createTabulator,
                    signedPayload, "reg-001", reg001PublicKey));

            Assertions.assertEquals(ErrorCodes.CANNOT_CREATE_EXPIRED_ENTITY, exception.getErrorCode());
        }
    }

    @Nested
    @SpringBootTest(classes = {JacksonConfig.class, CreateTabulatorBlockValidator.class, DldDataStore.class})
    class TabulatorAddedChain {

        private final CreateTabulatorBlockValidator blockValidator;
        private final ObjectMapper objectMapper;

        @Autowired
        TabulatorAddedChain(final ObjectMapper objectMapper) throws IOException {
            DldDataStore dldDataStore = new DldDataStore(objectMapper);
            this.blockValidator = new CreateTabulatorBlockValidator(dldDataStore, objectMapper);
            this.objectMapper = objectMapper;

            dldDataStore.loadDldChain(new ClassPathResource("chains/tabulator001.chain").getFile());
        }

        @Test
        @DisplayName("It should throw an exception if the writing user is not a registrar")
        void writerIsNotRegistrar() throws JsonProcessingException, NoSuchAlgorithmException {
            CreateTabulator createTabulator = new CreateTabulator()
                    .setTabulatorId("tabulator-001")
                    .setTabulatorPublicKey(CryptoUtil.encodePublicKeyToString(tabulator001PublicKey));

            String signedPayload = CryptoUtil.sign(objectMapper.writeValueAsString(createTabulator), tabulator001PrivateKey)
                    .orElseThrow();

            InvalidBlockException exception = Assertions.assertThrows(InvalidBlockException.class, () -> blockValidator.validate(createTabulator,
                    signedPayload, "tabulator-001", tabulator001PublicKey));

            Assertions.assertEquals(ErrorCodes.REGISTRAR_DOES_NOT_EXIST, exception.getErrorCode());
        }
    }
}
