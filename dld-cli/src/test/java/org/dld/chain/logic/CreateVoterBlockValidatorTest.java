package org.dld.chain.logic;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.dld.app.core.ErrorCodes;
import org.dld.app.data.DldDataStore;
import org.dld.app.json.JacksonConfig;
import org.dld.app.util.CryptoUtil;
import org.dld.chain.block.Block;
import org.dld.chain.block.CreateVoter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.ZonedDateTime;

@SpringBootTest(classes = {JacksonConfig.class, CreateVoterBlockValidator.class, DldDataStore.class})
public class CreateVoterBlockValidatorTest {

    private static PublicKey reg001PublicKey;
    private static PrivateKey genesis001PrivateKey;
    private static PrivateKey reg001PrivateKey;
    private static PublicKey voter001PublicKey;
    private static PrivateKey voter001PrivateKey;

    @BeforeAll
    static void loadKeys() throws NoSuchAlgorithmException, IOException {
        genesis001PrivateKey =
                CryptoUtil.loadPrivateKey(new ClassPathResource("keypairs/genesis-001/dld.key").getFile())
                        .orElseThrow();
        reg001PublicKey = CryptoUtil.loadPublicKey(new ClassPathResource("keypairs/reg-001/dld.pub").getFile())
                .orElseThrow();
        reg001PrivateKey = CryptoUtil.loadPrivateKey(new ClassPathResource("keypairs/reg-001/dld.key").getFile())
                .orElseThrow();
        voter001PublicKey = CryptoUtil.loadPublicKey(new ClassPathResource("keypairs/voter-001/dld.pub").getFile())
                .orElseThrow();
        voter001PrivateKey = CryptoUtil.loadPrivateKey(new ClassPathResource("keypairs/voter-001/dld.key").getFile())
                .orElseThrow();
    }

    @Nested
    @SpringBootTest(classes = {JacksonConfig.class, CreateVoterBlockValidator.class, DldDataStore.class})
    class RegistrarAddedChain {

        private final CreateVoterBlockValidator blockValidator;
        private final ObjectMapper objectMapper;

        @Autowired
        RegistrarAddedChain(final ObjectMapper objectMapper) throws IOException {
            DldDataStore dldDataStore = new DldDataStore(objectMapper);
            this.blockValidator = new CreateVoterBlockValidator(dldDataStore, objectMapper);
            this.objectMapper = objectMapper;

            dldDataStore.loadDldChain(new ClassPathResource("chains/reg001.chain").getFile());
        }

        @Test
        @DisplayName("It should successfully create a block when everything is present and valid")
        void createValidBlock() throws JsonProcessingException, NoSuchAlgorithmException {

            CreateVoter createVoter = new CreateVoter()
                    .setVoterId("voter-001")
                    .setVoterPublicKey(CryptoUtil.encodePublicKeyToString(voter001PublicKey));

            String signedPayload = CryptoUtil.sign(objectMapper.writeValueAsString(createVoter), reg001PrivateKey)
                    .orElseThrow();

            Block result = Assertions.assertDoesNotThrow(
                    () -> blockValidator.validate(createVoter, signedPayload, "reg-001", reg001PublicKey));

            Assertions.assertNotNull(result);
        }


        @Test
        @DisplayName("It should throw an exception if the voter id is missing")
        void missingId() throws JsonProcessingException, NoSuchAlgorithmException {
            CreateVoter createVoter = new CreateVoter()
                    .setVoterPublicKey(CryptoUtil.encodePublicKeyToString(voter001PublicKey));

            String signedPayload = CryptoUtil.sign(objectMapper.writeValueAsString(createVoter), genesis001PrivateKey)
                    .orElseThrow();

            InvalidBlockException exception = Assertions.assertThrows(InvalidBlockException.class, () -> blockValidator.validate(createVoter,
                    signedPayload, "reg-001", reg001PublicKey));

            Assertions.assertEquals(ErrorCodes.ENTITY_ID_MUST_BE_SPECIFIED, exception.getErrorCode());
        }

        @Test
        @DisplayName("It should throw an exception if the voter public key is missing")
        void missingPublicKey() throws JsonProcessingException, NoSuchAlgorithmException {
            CreateVoter createVoter = new CreateVoter()
                    .setVoterId("voter-001");

            String signedPayload = CryptoUtil.sign(objectMapper.writeValueAsString(createVoter), genesis001PrivateKey)
                    .orElseThrow();

            InvalidBlockException exception = Assertions.assertThrows(InvalidBlockException.class, () -> blockValidator.validate(createVoter,
                    signedPayload, "reg-001", reg001PublicKey));

            Assertions.assertEquals(ErrorCodes.ENTITY_PUBLIC_KEY_MUST_BE_SPECIFIED, exception.getErrorCode());
        }

        @Test
        @DisplayName("It should throw an exception if the user's expiry date is in the past")
        void voterExpiryInThePast() throws JsonProcessingException, NoSuchAlgorithmException {
            CreateVoter createVoter = new CreateVoter()
                    .setVoterId("voter-001")
                    .setVoterPublicKey(CryptoUtil.encodePublicKeyToString(voter001PublicKey))
                    .setExpires(ZonedDateTime.of(LocalDateTime.of(2000, Month.JANUARY, 1, 0, 0), ZoneId.systemDefault()));

            String signedPayload = CryptoUtil.sign(objectMapper.writeValueAsString(createVoter), genesis001PrivateKey)
                    .orElseThrow();

            InvalidBlockException exception = Assertions.assertThrows(InvalidBlockException.class, () -> blockValidator.validate(createVoter,
                    signedPayload, "reg-001", reg001PublicKey));

            Assertions.assertEquals(ErrorCodes.CANNOT_CREATE_EXPIRED_ENTITY, exception.getErrorCode());
        }
    }

    @Nested
    @SpringBootTest(classes = {JacksonConfig.class, CreateVoterBlockValidator.class, DldDataStore.class})
    class VoterAddedChain {

        private final CreateVoterBlockValidator blockValidator;
        private final ObjectMapper objectMapper;

        @Autowired
        VoterAddedChain(final ObjectMapper objectMapper) throws IOException {
            DldDataStore dldDataStore = new DldDataStore(objectMapper);
            this.blockValidator = new CreateVoterBlockValidator(dldDataStore, objectMapper);
            this.objectMapper = objectMapper;

            dldDataStore.loadDldChain(new ClassPathResource("chains/voter001.chain").getFile());
        }

        @Test
        @DisplayName("It should throw an exception if the writing user is not a registrar")
        void writerIsNotRegistrar() throws JsonProcessingException, NoSuchAlgorithmException {
            CreateVoter createVoter = new CreateVoter()
                    .setVoterId("voter-001")
                    .setVoterPublicKey(CryptoUtil.encodePublicKeyToString(voter001PublicKey));

            String signedPayload = CryptoUtil.sign(objectMapper.writeValueAsString(createVoter), voter001PrivateKey)
                    .orElseThrow();

            InvalidBlockException exception = Assertions.assertThrows(InvalidBlockException.class, () -> blockValidator.validate(createVoter,
                    signedPayload, "voter-001", voter001PublicKey));

            Assertions.assertEquals(ErrorCodes.REGISTRAR_DOES_NOT_EXIST, exception.getErrorCode());
        }
    }
}
