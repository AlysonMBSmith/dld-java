package org.dld.app.core;

import org.dld.app.commands.Entrypoint;
import org.dld.app.commands.handlers.DldExceptionHandler;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.ExitCodeGenerator;
import org.springframework.stereotype.Component;
import picocli.CommandLine;

@Component
public class ApplicationRunner implements CommandLineRunner, ExitCodeGenerator {

    private final Entrypoint entrypoint;

    private final CommandLine.IFactory factory;
    private int exitCode;

    public ApplicationRunner(final Entrypoint entrypoint, final CommandLine.IFactory factory) {
        this.entrypoint = entrypoint;
        this.factory = factory;
    }

    @Override
    public void run(String... args) {
        exitCode = new CommandLine(entrypoint, factory)
                .setParameterExceptionHandler(new DldExceptionHandler())
                .execute(args);
    }

    @Override
    public int getExitCode() {
        return exitCode;
    }
}
