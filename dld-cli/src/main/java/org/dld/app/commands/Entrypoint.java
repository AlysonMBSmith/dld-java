package org.dld.app.commands;

import org.springframework.stereotype.Component;
import picocli.CommandLine;

import java.util.concurrent.Callable;

@Component
@CommandLine.Command(name = "entrypoint", mixinStandardHelpOptions = true,
        subcommands = { GenerateKeysCommand.class, CreateDldChainCommand.class , CreateRegistrarCommand.class,
                CreateVoterCommand.class, CreateTabulatorCommand.class }
)
public class Entrypoint implements Callable<Integer> {

    @Override
    public Integer call() {
        System.err.println("Please specify a valid command.");
        return 1;
    }

}
