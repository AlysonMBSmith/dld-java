package org.dld.app.commands;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.dld.app.commands.handlers.PublicKeyConverter;
import org.dld.app.core.ErrorCodes;
import org.dld.app.data.BlockDecodingException;
import org.dld.app.data.DldDataStore;
import org.dld.app.data.types.Registrar;
import org.dld.app.util.CryptoUtil;
import org.dld.chain.block.Block;
import org.dld.chain.block.CreateRegistrar;
import org.dld.chain.logic.CreateRegistrarBlockValidator;
import org.springframework.beans.factory.annotation.Autowired;
import picocli.CommandLine;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.security.PublicKey;
import java.time.ZonedDateTime;

@CommandLine.Command(name = "create-registrar", mixinStandardHelpOptions = true)
public class CreateRegistrarCommand extends DLDCommand {

    @CommandLine.Option(names = { "--publickey", "-p" }, required = true, converter = PublicKeyConverter.class,
            description = "Path to the public key file to be used for this operation")
    private PublicKey publicKey;

    @CommandLine.Option(names = { "--regid" }, required = true, description = "The ID to use for the registrar")
    private String registrarId;

    @CommandLine.Option(names = { "-s" }, description = "Specify this flag if making this registrar a super registrar")
    private boolean isSuperRegistrar;

    @CommandLine.Option(names = { "--expiry", "-e" }, description = "Optional expiry date-time for a registrar " +
            "(ISO-8601 zoned date time)")
    private ZonedDateTime expiry;

    private final CreateRegistrarBlockValidator createRegistrarBlockValidator;

    @Autowired
    CreateRegistrarCommand(final ObjectMapper jsonMapper, final DldDataStore dldDataStore,
            final CreateRegistrarBlockValidator createRegistrarBlockValidator) {
        super(jsonMapper, dldDataStore);
        this.createRegistrarBlockValidator = createRegistrarBlockValidator;
    }

    @Override
    public Integer call() throws Exception {
        try {
            dldDataStore.loadDldChain(new File(blockChainFileName));
        } catch (IOException e) {
            System.err.println("An IOException occurred while trying to read from the chain file." + e.getLocalizedMessage());
            return ErrorCodes.FILE_IO_ERROR.getCode();
        } catch (BlockDecodingException e) {
            System.err.println("Block chain decoding failed: " + e.getLocalizedMessage());
            return ErrorCodes.BLOCK_CHAIN_DECODE_FAILED.getCode();
        }

        Registrar superRegistrar = dldDataStore.getRegistrarDataStore().retrieve(userId).orElse(null);
        if (superRegistrar == null) {
            return ErrorCodes.REGISTRAR_DOES_NOT_EXIST.getCode();
        }

        if (!CryptoUtil.doesKeyPairMatch(privateKey, superRegistrar.getPublicKey())) {
            return ErrorCodes.KEY_PAIR_MISMATCH.getCode();
        }

        CreateRegistrar createRegistrar = new CreateRegistrar()
                .setParentBlockSignature(dldDataStore.getHeadBlockSignature())
                .setSuperRegistrar(isSuperRegistrar)
                .setRegistrarId(registrarId)
                .setRegistrarPublicKey(CryptoUtil.encodePublicKeyToString(publicKey))
                .setExpires(expiry);

        String signedPayload = signPayload(createRegistrar, privateKey).orElse(null);
        if (signedPayload == null) {
            System.err.println("Payload signing failed.");
            return ErrorCodes.UNABLE_TO_SIGN_PAYLOAD.getCode();
        }

        PublicKey writerPublicKey = determinePublicKey(createRegistrar, userId).orElse(null);
        if (writerPublicKey == null) {
            return ErrorCodes.WRITER_PUBLIC_KEY_NOT_FOUND.getCode();
        }
        Block createRegistrarBlock = createRegistrarBlockValidator.validate(createRegistrar, signedPayload, userId,
                writerPublicKey);

        File dldChainFile = new File(blockChainFileName);

        try (FileWriter writer = new FileWriter(dldChainFile, true)) {
            writer.append(jsonMapper.writeValueAsString(createRegistrarBlock));
            writer.append('\n');
        } catch (IOException e) {
            System.err.println("An error occurred while writing the DLD Chain: " + e.getLocalizedMessage());
            return ErrorCodes.FILE_IO_ERROR.getCode();
        }

        return 0;
    }
}
