package org.dld.app.commands.handlers;

import org.dld.app.commands.exceptions.UnableToLoadPrivateKeyException;
import org.dld.app.core.ErrorCodes;
import picocli.CommandLine;

import java.security.NoSuchAlgorithmException;

public class DldExceptionHandler implements CommandLine.IParameterExceptionHandler {
    @Override
    public int handleParseException(final CommandLine.ParameterException exception, final String[] args) {
        if (exception.getCause() instanceof UnableToLoadPrivateKeyException) {
            return ErrorCodes.UNABLE_TO_LOAD_PRIVATE_KEY.getCode();
        }

        if (exception.getCause() instanceof NoSuchAlgorithmException) {
            return ErrorCodes.RSA_NOT_FOUND.getCode();
        }

        return ErrorCodes.UNCATEGORIZED_ERROR.getCode();
    }
}
