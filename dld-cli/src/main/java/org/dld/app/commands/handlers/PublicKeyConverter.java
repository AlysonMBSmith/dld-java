package org.dld.app.commands.handlers;

import org.dld.app.commands.exceptions.UnableToLoadPublicKeyException;
import org.dld.app.util.CryptoUtil;
import picocli.CommandLine;

import java.io.File;
import java.security.PublicKey;

public class PublicKeyConverter implements CommandLine.ITypeConverter<PublicKey>{
    @Override
    public PublicKey convert(final String fileName) throws Exception {
        PublicKey publicKey = CryptoUtil.loadPublicKey(new File(fileName)).orElse(null);

        if (publicKey == null) {
            throw new UnableToLoadPublicKeyException();
        }

        return publicKey;
    }
}
