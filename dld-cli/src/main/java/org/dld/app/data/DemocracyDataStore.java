package org.dld.app.data;

import org.dld.app.data.types.LegislationProposal;
import org.dld.app.data.types.Vote;
import org.dld.app.data.types.VoteDelegation;
import org.dld.app.data.types.VoteResult;
import org.dld.app.data.types.VoteTally;
import org.dld.chain.block.DelegateVote;
import org.dld.chain.block.ProposeLegislation;
import org.dld.chain.block.VotePayload;
import org.dld.chain.block.VoteResultPayload;
import org.dld.chain.block.VoteTallyPayload;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class DemocracyDataStore {

    private final Map<String, LegislationProposal> activeLegislationProposals;
    private final Set<VoteDelegation> generalDelegations;

    public DemocracyDataStore() {
        this.activeLegislationProposals = new HashMap<>();
        this.generalDelegations = new HashSet<>();
    }

    void handle(final ProposeLegislation proposeLegislation, final String payloadSignature) {
        LegislationProposal legislationProposal = LegislationProposal.create(proposeLegislation, payloadSignature);
        this.activeLegislationProposals.put(payloadSignature, legislationProposal);
    }

    void handle(final DelegateVote delegateVote) {
        VoteDelegation voteDelegation = VoteDelegation.create(delegateVote);
        if (delegateVote.getProposalSignature() != null) {
            LegislationProposal legislationProposal = this.activeLegislationProposals.get(delegateVote.getProposalSignature());
            if (legislationProposal == null) {
                throw new BlockDecodingException("A voter delegated their vote on a proposal that does not exist in " +
                        "the chain.");
            }

            legislationProposal.add(voteDelegation);
        } else {
            this.generalDelegations.add(voteDelegation);
        }
    }

    void handle(final VotePayload votePayload) {
        Vote vote = Vote.create(votePayload);
        LegislationProposal legislationProposal = this.activeLegislationProposals.get(vote.getProposalSignature());
        if (legislationProposal == null) {
            throw new BlockDecodingException("A voter voted on a proposal that does not exist in the chain.");
        }
        legislationProposal.add(vote);
    }

    void handle(final VoteTallyPayload voteTallyPayload) {
        VoteTally voteTally = VoteTally.create(voteTallyPayload);
        LegislationProposal legislationProposal = this.activeLegislationProposals.get(voteTally.getProposalSignature());
        if (legislationProposal == null) {
            throw new BlockDecodingException("A vote tally exists for a proposal that does not exist in the chain.");
        }

        legislationProposal.add(voteTally);
    }

    void handle(final VoteResultPayload voteResultPayload) {
        VoteResult voteResult = VoteResult.create(voteResultPayload);
        LegislationProposal legislationProposal = this.activeLegislationProposals.get(voteResult.getProposalSignature());
        if (legislationProposal == null) {
            throw new BlockDecodingException("A vote tally exists for a proposal that does not exist in the chain.");
        }

        legislationProposal.setVoteResult(voteResult);
    }
}
