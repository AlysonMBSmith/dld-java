package org.dld.app.data;

import org.dld.app.data.types.Registrar;
import org.dld.chain.block.CreateRegistrar;
import org.dld.chain.block.Genesis;
import org.dld.chain.block.UpdateRegistrar;

import java.security.PublicKey;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class RegistrarDataStore {

    private final Map<String, Registrar> registrarMap;
    private Registrar genesisSuperRegistrar;

    public RegistrarDataStore() {
        registrarMap = new HashMap<>();
    }

    /**
     * Retrieve the registrar associated with the provided id.
     *
     * @param id The id of the registrar to be retrieved.
     * @return An Optional containing the registrar if it exists in the store.
     */
    public Optional<Registrar> retrieve(final String id) {
        return Optional.ofNullable(registrarMap.get(id));
    }

    /**
     * Determine whether a registrar exists with the provided id.
     *
     * @param id The registrar id.
     * @return True if the data store contains a registrar with the given id.
     */
    public boolean contains(final String id) {
        return registrarMap.containsKey(id);
    }

    /**
     * Retrieve the public key associated with the registrar with the given id.
     *
     * @param registrarId The registrar's id.
     * @return An Optional containing the most recent public key associated with the id parameter if found.
     */
    public Optional<PublicKey> getRegistrarPublicKey(final String registrarId) {
        return Optional.ofNullable(registrarMap.get(registrarId)).map(Registrar::getPublicKey);
    }

    public PublicKey getGenesisSuperRegistrarPublicKey() {
        return genesisSuperRegistrar.getPublicKey();
    }

    private void setGenesisSuperRegistrar(final Registrar genesisSuperRegistrar) {
        if (this.genesisSuperRegistrar != null) {
            throw new IllegalStateException("The genesis super registrar has already been written");
        }
        this.genesisSuperRegistrar = genesisSuperRegistrar;
    }

    void handle(final Genesis genesisBlock) {
        Registrar genesisRegistrar = Registrar.create(genesisBlock);
        setGenesisSuperRegistrar(genesisRegistrar);
        this.registrarMap.put(genesisRegistrar.getId(), genesisRegistrar);
    }

    void handle(final CreateRegistrar createRegistrar) {
        Registrar registrar = Registrar.create(createRegistrar);
        this.registrarMap.put(registrar.getId(), registrar);
    }

    void handle(final UpdateRegistrar updateRegistrar) {
        Registrar registrar = Registrar.create(updateRegistrar);
        this.registrarMap.put(registrar.getId(), registrar);
    }
}
