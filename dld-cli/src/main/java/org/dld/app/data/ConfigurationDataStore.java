package org.dld.app.data;

import org.dld.chain.block.Genesis;
import org.dld.chain.block.UpdateSpecVersion;

public class ConfigurationDataStore {

    private String specVersion;

    public String getSpecVersion() {
        return this.specVersion;
    }

    void handle(final Genesis genesisBlock) {
        this.specVersion = genesisBlock.getSpecVersion();
    }

    void handle(final UpdateSpecVersion updateSpecVersion) {
        this.specVersion = updateSpecVersion.getSpecVersion();
    }
}
