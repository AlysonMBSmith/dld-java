package org.dld.app.data;

import org.dld.app.data.types.Registrar;
import org.dld.app.data.types.Voter;
import org.dld.chain.block.CreateVoter;
import org.dld.chain.block.UpdateVoter;

import java.security.PublicKey;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class VoterDataStore {

    private final Map<String, Voter> voterMap;

    public VoterDataStore() {
        voterMap = new HashMap<>();
    }

    /**
     * Retrieve the voter associated with the provided id.
     *
     * @param id The id of the voter to be retrieved.
     * @return An Optional containing the voter if it exists in the store.
     */
    public Optional<Voter> retrieve(final String id) {
        return Optional.ofNullable(voterMap.get(id));
    }

    /**
     * Determine whether a voter exists with the provided id.
     *
     * @param id The voter id.
     * @return True if the data store contains a voter with the given id.
     */
    public boolean contains(final String id) {
        return voterMap.containsKey(id);
    }

    /**
     * Retrieve the public key associated with the voter with the given id.
     *
     * @param voterId The voter's id.
     * @return An Optional containing the most recent public key associated with the id parameter if found.
     */
    public Optional<PublicKey> getVoterPublicKey(final String voterId) {
        return Optional.ofNullable(voterMap.get(voterId)).map(Voter::getPublicKey);
    }

    void handle(final CreateVoter createVoter) {
        Voter voter = Voter.create(createVoter);
        voterMap.put(voter.getId(), voter);
    }

    void handle(final UpdateVoter updateVoter) {
        Voter voter = Voter.create(updateVoter);
        voterMap.put(voter.getId(), voter);
    }
}
