package org.dld.app.data.types;

import org.dld.app.data.BlockDecodingException;
import org.dld.app.util.CryptoUtil;
import org.dld.chain.block.CreateRegistrar;
import org.dld.chain.block.Genesis;
import org.dld.chain.block.UpdateRegistrar;

import java.security.PublicKey;
import java.time.ZonedDateTime;

public class Registrar {

    private final String id;
    private final boolean isSuperRegistrar;
    private final boolean isGenesis;
    private final PublicKey publicKey;
    private final ZonedDateTime expires;

    public Registrar(final String id, final boolean isSuperRegistrar, final boolean isGenesis,
            final PublicKey publicKey,
            final ZonedDateTime expires) {
        this.id = id;
        this.isSuperRegistrar = isSuperRegistrar;
        this.isGenesis = isGenesis;
        this.publicKey = publicKey;
        this.expires = expires;
    }

    public String getId() {
        return id;
    }

    public boolean isSuperRegistrar() {
        return isSuperRegistrar;
    }

    public boolean isGenesis() {
        return isGenesis;
    }

    public PublicKey getPublicKey() {
        return publicKey;
    }

    public ZonedDateTime getExpires() {
        return expires;
    }

    public static Registrar create(final Genesis genesis) {
        return new Registrar(genesis.getGenesisSuperRegistrarId(), true, true,
                CryptoUtil.decodePublicKeyFromString(genesis.getGenesisSuperRegistrarPublicKey())
                        .orElseThrow(() -> new BlockDecodingException("Unable to decode genesis block's public key")),
                null);
    }

    public static Registrar create(final CreateRegistrar createRegistrar) {
        return new Registrar(createRegistrar.getRegistrarId(), createRegistrar.isSuperRegistrar(), false,
                CryptoUtil.decodePublicKeyFromString(createRegistrar.getRegistrarPublicKey())
                        .orElseThrow(() -> new BlockDecodingException("Unable to decode registrar's public key")),
                createRegistrar.getExpires());
    }

    public static Registrar create(final UpdateRegistrar updateRegistrar) {
        return new Registrar(updateRegistrar.getRegistrarId(), updateRegistrar.isSuperRegistrar(), false,
                CryptoUtil.decodePublicKeyFromString(updateRegistrar.getRegistrarPublicKey())
                        .orElseThrow(() -> new BlockDecodingException("Unable to decode registrar's public key")),
                updateRegistrar.getExpires());
    }

    @Override
    public String toString() {
        return "Registrar{" +
                "id='" + id + '\'' +
                ", isSuperRegistrar=" + isSuperRegistrar +
                ", isGenesis=" + isGenesis +
                ", publicKey=" + publicKey +
                ", expires=" + expires +
                '}';
    }
}
