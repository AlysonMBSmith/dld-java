package org.dld.app.data.types;

import org.dld.chain.block.VoteResultPayload;

import java.util.List;

public class VoteResult {

    private final String tabulatorId;
    private final List<String> tabulatorConsensus;
    private final boolean proposalPassed;
    private final String proposalSignature;

    public VoteResult(final VoteResultPayload voteResultPayload) {
        this.tabulatorId = voteResultPayload.getTabulatorId();
        this.tabulatorConsensus = voteResultPayload.getTabulatorConsensus();
        this.proposalPassed = voteResultPayload.isProposalPassed();
        this.proposalSignature = voteResultPayload.getProposalSignature();
    }

    public String getTabulatorId() {
        return tabulatorId;
    }

    public List<String> getTabulatorConsensus() {
        return tabulatorConsensus;
    }

    public boolean isProposalPassed() {
        return proposalPassed;
    }

    public String getProposalSignature() {
        return proposalSignature;
    }

    @Override
    public String toString() {
        return "VoteResult{" +
                "tabulatorId='" + tabulatorId + '\'' +
                ", tabulatorConsensus=" + tabulatorConsensus +
                ", proposalPassed=" + proposalPassed +
                ", proposalSignature='" + proposalSignature + '\'' +
                '}';
    }

    public static VoteResult create(final VoteResultPayload voteResultPayload) {
        return new VoteResult(voteResultPayload);
    }
}
