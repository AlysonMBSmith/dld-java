package org.dld.app.data.types;

import org.dld.app.data.BlockDecodingException;
import org.dld.app.util.CryptoUtil;
import org.dld.chain.block.CreateVoter;
import org.dld.chain.block.UpdateVoter;

import java.security.PublicKey;
import java.time.ZonedDateTime;

public class Voter {

    private final String id;
    private final PublicKey publicKey;
    private final ZonedDateTime expires;

    private Voter(final String id, final PublicKey publicKey, final ZonedDateTime expires) {
        this.id = id;
        this.publicKey = publicKey;
        this.expires = expires;
    }

    public String getId() {
        return id;
    }

    public PublicKey getPublicKey() {
        return publicKey;
    }

    public ZonedDateTime getExpires() {
        return expires;
    }

    @Override
    public String toString() {
        return "Voter{" +
                "id='" + id + '\'' +
                ", publicKey=" + publicKey +
                ", expires=" + expires +
                '}';
    }

    public static Voter create(final CreateVoter createVoter) {
        return new Voter(createVoter.getVoterId(),
                CryptoUtil.decodePublicKeyFromString(createVoter.getVoterPublicKey())
                        .orElseThrow(() -> new BlockDecodingException("Unable to decode voter's public key")),
                createVoter.getExpires());
    }

    public static Voter create(final UpdateVoter updateVoter) {
        return new Voter(updateVoter.getVoterId(),
                CryptoUtil.decodePublicKeyFromString(updateVoter.getVoterPublicKey())
                        .orElseThrow(() -> new BlockDecodingException("Unable to decode voter's public key")),
                updateVoter.getExpires());
    }

}
