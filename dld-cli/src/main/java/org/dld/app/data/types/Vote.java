package org.dld.app.data.types;

import org.dld.chain.block.VotePayload;

import java.util.Objects;

public class Vote {

    private final String id;
    private final String proposalSignature;
    private final boolean vote;

    public Vote(final String id, final String proposalSignature, final boolean vote) {
        this.id = id;
        this.proposalSignature = proposalSignature;
        this.vote = vote;
    }

    public String getId() {
        return id;
    }

    public String getProposalSignature() {
        return proposalSignature;
    }

    public boolean getVote() {
        return vote;
    }

    @Override
    public String toString() {
        return "Vote{" +
                "id='" + id + '\'' +
                ", proposalSignature='" + proposalSignature + '\'' +
                ", vote=" + vote +
                '}';
    }

    @Override
    public boolean equals(final Object other) {
        if (this == other) {
            return true;
        }
        if (other == null || getClass() != other.getClass()) {
            return false;
        }
        Vote otherVote = (Vote) other;
        return vote == otherVote.vote && id.equals(otherVote.id) && proposalSignature.equals(otherVote.proposalSignature);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, proposalSignature, vote);
    }

    public static Vote create(final VotePayload votePayload) {
        return new Vote(votePayload.getVoterId(), votePayload.getProposalSignature(), votePayload.getVote());
    }
}
