package org.dld.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import picocli.CommandLine;
import picocli.spring.PicocliSpringFactory;

@ComponentScan(basePackages = {"org.dld.app.core", "org.dld.app.commands", "org.dld.app.json", "org.dld.app.data",
        "org.dld.chain.logic"})
@SpringBootApplication
public class ApplicationBootstrap {

    @Bean
    CommandLine.IFactory factory(final ApplicationContext context) {
        return new PicocliSpringFactory(context);
    }

    public static void main(String[] args) {
        System.exit(SpringApplication.exit(SpringApplication.run(ApplicationBootstrap.class, args)));
    }

}
