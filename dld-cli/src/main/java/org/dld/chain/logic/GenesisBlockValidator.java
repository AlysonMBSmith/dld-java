package org.dld.chain.logic;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.dld.app.core.ErrorCodes;
import org.dld.app.data.DldDataStore;
import org.dld.app.util.GitUtil;
import org.dld.chain.block.Genesis;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;

@Component
public class GenesisBlockValidator extends BlockValidator<Genesis> {

    @Autowired
    public GenesisBlockValidator(final DldDataStore dldDataStore, final ObjectMapper objectMapper) {
        super(dldDataStore, objectMapper);
    }

    @Override
    void validateUserCanWrite(final Genesis genesis, final String writerId) {
        // Nothing to do
    }

    @Override
    void validate(final Genesis payload) throws InvalidBlockException {
        Repository repository = GitUtil.getGitRepo(new File(payload.getLegislationRepositorySourceURI()))
                .orElse(null);

        if (repository == null) {
            throw new InvalidBlockException(ErrorCodes.GIT_REPO_NOT_FOUND);
        }

        Ref mainlineHeadRef = GitUtil.getRefObject(repository, "refs/heads/" + payload.getLegislationRepositoryMainlineBranchName())
                .orElse(null);

        if (mainlineHeadRef == null) {
            throw new InvalidBlockException(ErrorCodes.GIT_MAINLINE_REF_NOT_FOUND);
        }

        if (!mainlineHeadRef.getObjectId().getName().equals(payload.getLegislationRepositoryMainlineHeadHash())) {
            throw new InvalidBlockException(ErrorCodes.GIT_HASH_DOES_NOT_MATCH);
        }
    }
}
