package org.dld.chain.logic;

import org.dld.app.core.ErrorCodes;

public class InvalidBlockException extends RuntimeException {

    private final ErrorCodes errorCode;

    public InvalidBlockException() {
        super();
        this.errorCode = null;
    }

    public InvalidBlockException(final String message) {
        super(message);
        this.errorCode = null;
    }

    public InvalidBlockException(final String message, final Throwable cause) {
        super(message, cause);
        this.errorCode = null;
    }

    public InvalidBlockException(final Throwable cause) {
        super(cause);
        this.errorCode = null;
    }

    public InvalidBlockException(final ErrorCodes errorCode) {
        super();
        this.errorCode = errorCode;
    }

    public InvalidBlockException(final ErrorCodes errorCode, final String message) {
        super(message);
        this.errorCode = errorCode;
    }

    public InvalidBlockException(final ErrorCodes errorCode, final String message, final Throwable cause) {
        super(message, cause);
        this.errorCode = null;
    }

    public InvalidBlockException(final ErrorCodes errorCode, final Throwable cause) {
        super(cause);
        this.errorCode = null;
    }

    public ErrorCodes getErrorCode() {
        return errorCode;
    }
}
